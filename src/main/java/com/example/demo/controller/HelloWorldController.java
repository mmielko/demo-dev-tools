package com.example.demo.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {

    @ResponseBody
    @GetMapping(path = "/hello", produces = MediaType.TEXT_PLAIN_VALUE, consumes = {MediaType.ALL_VALUE})
    public String test(){
        return "Hello world";
    }
}
